/*
 * counter-code.c
 *
 * Created: 6/11/2020 7:29:35 PM
 * Author : Ali-Hejazi
 */ 

#define F_CPU 8000000
#include <avr/io.h>
#include <util/delay.h>


int main(void)
{
	DDRC = 0xFF;
	PORTC = 0xFF;
	DDRA &= ~(1 << PA1); 
	uint8_t count=0;

	
	while (1)
	{
		if (1)
		{	
			switch(count)
			{
				case 0:
				PORTC = 0b00000000;
				break;
				
				case 1:
				PORTC = 0b00000001;
				break;
				
				case 2:
				PORTC = 0b00000010;
				break;
				
				case 3:
				PORTC = 0b00000011;
				break;
				
				case 4:
				PORTC = 0b00000100;
				break;
			
				case 5:
				PORTC = 0b00000101;
				break;
			
				case 6:
				PORTC = 0b00000110;
				break;
			
				case 7:
				PORTC = 0b00000111;
				break;
				
				case 8:
				PORTC = 0b00001000;
				break;
				
				case 9:
				PORTC = 0b00001001;
				break;
				
				case 10:
				PORTC = 0b00010000;
				break;
				
				case 11:
				PORTC = 0b00010001;
				break;
				
				case 12:
				PORTC = 0b00010010;
				break;
				
				case 13:
				PORTC = 0b00010011;
				break;
				
				case 14:
				PORTC = 0b00010100;
				break;
				
				case 15:
				PORTC = 0b00010101;
				break;
				
				case 16:
				PORTC = 0b00010110;
				break;
				
				case 17:
				PORTC = 0b00010111;
				break;
				
				case 18:
				PORTC = 0b00011000;
				break;
				
				case 19:
				PORTC = 0b00011001;
				break;
				
				case 20:
				PORTC = 0b00100000;
				break;
				
				case 21:
				PORTC = 0b00100001;
				break;
				
				case 22:
				PORTC = 0b00100010;
				break;
				
				case 23:
				PORTC = 0b00100011;
				break;
				
				case 24:
				PORTC = 0b00100100;
				break;
				
				case 25:
				PORTC = 0b00100101;
				break;
				
				case 26:
				PORTC = 0b00100110;
				break;
				
				case 27:
				PORTC = 0b00100111;
				break;
				
				case 28:
				PORTC = 0b00101000;
				break;
				
				case 29:
				PORTC = 0b00101001;
				break;
				
				case 30:
				PORTC = 0b00110000;
				break;
				
				case 31:
				PORTC = 0b00110001;
				break;
				
				case 32:
				PORTC = 0b00110010;
				break;
				
				case 33:
				PORTC = 0b00110011;
				break;
				
				case 34:
				PORTC = 0b00110100;
				break;
				
				case 35:
				PORTC = 0b00110101;
				break;
				
				case 36:
				PORTC = 0b00110110;
				break;
				
				case 37:
				PORTC = 0b00110111;
				break;
				
				case 38:
				PORTC = 0b00111000;
				break;
				
				case 39:
				PORTC = 0b00111001;
				break;
			
				case 40:
				PORTC = 0b01000000;
				break;
				
				case 41:
				PORTC = 0b01000001;
				break;
				
				case 42:
				PORTC = 0b01000010;
				break;
				
				case 43:
				PORTC = 0b01000011;
				break;
				
				case 44:
				PORTC = 0b01000100;
				break;
				
				case 45:
				PORTC = 0b01000101;
				break;
				
				case 46:
				PORTC = 0b01000110;
				break;
				
				case 47:
				PORTC = 0b01000111;
				break;
				
				case 48:
				PORTC = 0b01001000;
				break;
				
				case 49:
				PORTC = 0b01001001;
				break;
				
				case 50:
				PORTC = 0b01010000;
				
				case 51:
				PORTC = 0b01010001;
				break;
				
				case 52:
				PORTC = 0b01010010;
				break;
				
				case 53:
				PORTC = 0b01010011;
				break;
				
				case 54:
				PORTC = 0b01010100;
				break;
				
				case 55:
				PORTC = 0b01010101;
				break;
				
				case 56:
				PORTC = 0b01010110;
				break;
				
				case 57:
				PORTC = 0b01010111;
				break;
				
				case 58:
				PORTC = 0b01011000;
				break;
				
				case 59:
				PORTC = 0b01011001;
				break;
			
				case 60:
				PORTC = 0b01100000;
				break;
				
				case 61:
				PORTC = 0b01100001;
				break;
				
				case 62:
				PORTC = 0b01100010;
				break;
				
				case 63:
				PORTC = 0b01100011;
				break;
				
				case 64:
				PORTC = 0b01100100;
				break;
				
				case 65:
				PORTC = 0b01100101;
				break;
				
				case 66:
				PORTC = 0b01100110;
				break;
				
				case 67:
				PORTC = 0b01100111;
				break;
				
				case 68:
				PORTC = 0b01101000;
				break;
				
				case 69:
				PORTC = 0b01101001;
				break;
				
				case 70:
				PORTC = 0b01110000;
				break;
				
				case 71:
				PORTC = 0b01110001;
				break;
				
				case 72:
				PORTC = 0b01110010;
				break;
				
				case 73:
				PORTC = 0b01110011;
				break;
				
				case 74:
				PORTC = 0b01110100;
				break;
				
				case 75:
				PORTC = 0b01110101;
				break;
				
				case 76:
				PORTC = 0b01110110;
				break;
				
				case 77:
				PORTC = 0b01110111;
				break;
				
				case 78:
				PORTC = 0b01111000;
				break;
				
				case 79:
				PORTC = 0b01111001;
				break;
				
				case 80:
				PORTC = 0b10000000;
				
				case 81:
				PORTC = 0b10000001;
				break;
				
				case 82:
				PORTC = 0b10000010;
				break;
				
				case 83:
				PORTC = 0b10000011;
				break;
				
				case 84:
				PORTC = 0b10000100;
				break;
				
				case 85:
				PORTC = 0b10000101;
				break;
				
				case 86:
				PORTC = 0b10000110;
				break;
				
				case 87:
				PORTC = 0b10000111;
				break;
				
				case 88:
				PORTC = 0b10001000;
				break;
				
				case 89:
				PORTC = 0b10001001;
				break;
				
				case 90:
				PORTC = 0b10010000;
				break;
				
				case 91:
				PORTC = 0b10010001;
				break;
				
				case 92:
				PORTC = 0b10010010;
				break;
				
				case 93:
				PORTC = 0b10010011;
				break;
				
				case 94:
				PORTC = 0b10010100;
				break;
				
				case 95:
				PORTC = 0b10010101;
				break;
				
				case 96:
				PORTC = 0b10010110;
				break;
				
				case 97:
				PORTC = 0b10010111;
				break;
				
				case 98:
				PORTC = 0b10011000;
				break;
				
				case 99:
				PORTC = 0b10011001;
				break;				
			}
			if (PINA & (1<<PA1))
			{
				if (count != 100)
				{
					count++;
				}
			}
			else
			{
				if(count != 0)
				{
					count--;
				}
			}
		}
		_delay_ms(500);
		
	}
}
